"""
    Settings to be overwriten and used by API
"""
from os import environ
try:
    from dotenv import load_dotenv, find_dotenv
    load_dotenv(find_dotenv())
except Exception:
    pass

MONGO_HOST = environ.get("MONGO_HOST", 'localhost')
# MONGO_PORT = int(environ.get("MONGO_PORT"))
MONGO_DBNAME = environ.get("MONGO_DBNAME", 'eduqc')

# Skip these if your db has no auth. But it really should.
# MONGO_USERNAME = environ.get("MONGO_USER")
# MONGO_PASSWORD = environ.get("MONGO_PASS")
# MONGO_AUTH_SOURCE = environ.get("MONGO_USER")

MONGO_QUERY_BLACKLIST = ['$where']
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']

XML = False
JSON = True
X_DOMAINS = '*'
X_HEADERS = ['Authorization', 'Content-type', 'Cache-Control', 'If-Match']

CACHE_EXPIRES = 10
# PAGINATION_LIMIT = 1000
PAGINATION_DEFAULT = 30
AUTO_CREATE_LISTS = True
SOFT_DELETE = True


# --- Schemas ---
# pylint: disable=invalid-name

person_schema = {
    'email': {
        'type': 'string',
        'unique': True,
        'required': True,
        'regex':r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
    },
    'avatar': {'type': 'string', 'default': "statics/man.jpg"},
    'name': {'type': 'string', 'required': True},
    'surname': {'type': 'string'},
    'mother_name': {'type': 'string'},
    'father_name': {'type': 'string'},
    'cpf': {
        'type': 'dict', 'required': False , 'nullable': True,
        'schema': {
            'cpf_number': {'type': 'string', 'unique': True},
            'cpf_image': {'type': 'media'}
        }
    },
    'pis': {
        'type': 'list', 'required': False , 'nullable': True,
        'schema': {
            'type': 'dict',
            'schema': {
                'pis_number': {'type': 'string'},
                'pis_image': {'type': 'media'}
            }
        }
    },
    'passport': {
        'type': 'dict', 'required': False,
        'schema': {
            'passport_number': {'type': 'string'},
            'expedition_date': {'type': 'datetime'},
            'expire_date': {'type': 'datetime'},
            'passport_type': {'type': 'string'},
            'passport_country': {'type': 'string'}
        }
    },
    'election_document': {
        'type': 'dict', 'required': False,
        'schema': {
            'election_number': {'type': 'string'},
            'zone': {'type': 'string'},
            'section': {'type': 'string'},
            'expedition_date': {'type': 'datetime'},
            'election_image': {'type': 'media'}
        }
    },
    'drive_license': {
        'type': 'dict',
        'schema': {
            'drive_number': {'type': 'string'},
            'expedition_date': {'type': 'datetime'},
            'expire_date': {'type': 'datetime'},
            'drive_permission': {'type': 'string', 'allowed': ['A', 'B', 'C', 'D', "E"]},
            'drive_license_image': {'type': 'media'}
        }
    },
    'birth_data': {
        'type': 'dict',
        'schema': {
            'birth_state': {'type': 'string'},
            'birth_city': {'type': 'string'},
            'birth_country': {'type': 'string'},
            'birth_date': {'type': 'datetime', 'required': False, 'nullable': True}
        }
    },
    'gender': {'type': 'string', 'required': False, 'allowed':['M', 'F', 'N', 'P']},
}

person = {
    'schema': person_schema,
}

DOMAIN = {
    'person': person
}
