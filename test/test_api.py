import unittest
from eve import Eve
import os
from api import app
import settings
from base64 import b64encode
from os import environ, path
from pymongo.errors import ServerSelectionTimeoutError
import json
import io

try:
    load_dotenv(find_dotenv())
except Exception as e:
    pass


class basicEveTest(unittest.TestCase):
    """Basic Eve test class
    It should let a client available and a app.
    Make requests with:

    self.test_client.get('/endpoit')

    """

    def setUp(self):
        dir_path = path.dirname(path.realpath(__file__))
        # self.app = Eve(settings=dir_path+'/../settings.py')
        self.app = app
        self.test_client = self.app.test_client()
        hash = bytes(environ.get("MONGO_USER") + ':' +
                     environ.get("MONGO_PASS"), "utf-8")
        self.headers = {
            'Authorization': 'Basic %s' % b64encode(hash).decode("ascii"),
            'Content-Type': 'application/json'
        }
           

    def tearDown(self):
        with self.app.app_context():
            self.app.data.pymongo().db['person'].delete_many({})
    
    def get(self, url):
        try:
            response = self.test_client.get(url, headers=self.headers)
            return response
        except ServerSelectionTimeoutError as e:
            self.skipTest(str(response.response))

    def post(self, url, data):
        try:
            response = self.test_client.post(url, headers=self.headers, data=data)
            return response
        except ServerSelectionTimeoutError as e:
            self.skipTest(str(response.response))

    def test_user_sample(self):
        user = {
            'email': 'testando@teste.com',
            'name': 'Teste',
            'surname': 'Familia Teste'
        }
        response = self.post('person', json.dumps(user))
        self.assertEqual(response.status_code, 201, response.json)
    
    def test_user_birth_data(self):
        dir_path = os.path.dirname(__file__)
        local_file_to_send = os.path.join(dir_path, 'cpf_teste.jpg')
        image_cpf = open(os.path.join(dir_path, 'cpf_teste.jpg'), 'rb')
        user = {
            'email': 'testando2@teste.com',
            'name': 'Teste',
            'surname': 'Familia Teste',
            'gender': 'M',
            'birth_data': {
                'birth_state': 'MG',
                'birth_city': 'Patos de Minas',
                'birth_country': 'BRA',
                'birth_date': 'Tue, 02 Apr 2013 10:29:13 GMT'
            }
        }
        response = self.test_client.post('person', data=json.dumps(user), headers=self.headers)
        self.assertEqual(response.status_code, 201, response.json)
    
    def test_user_pis_data(self):
        user = {
            'email': 'testando2@teste.com',
            'name': 'Teste',
            'surname': 'Familia Teste',
            'pis': [
                {
                    'pis_number': '85548284392'
                }
            ],
        }
        response = self.test_client.post('person', data=json.dumps(user), headers=self.headers)
        self.assertEqual(response.status_code, 201, response.json)
    
    def test_user_drive_license_data(self):
        user = {
            'email': 'testando2@teste.com',
            'name': 'Teste',
            'surname': 'Familia Teste',
            'drive_license': {
                'drive_number': '07036438804',
                'expedition_date': 'Tue, 02 Apr 2013 10:29:13 GMT',
                'expire_date': 'Fri, 08 Mar 2019 21:23:52 GMT',
                'drive_permission': 'B',
            }
        }
        response = self.test_client.post('person', data=json.dumps(user), headers=self.headers)
        self.assertEqual(response.status_code, 201, response.json)
        user['drive_license']['drive_permission']  = 'Q'
        response = self.test_client.post('person', data=json.dumps(user), headers=self.headers)
        self.assertEqual(response.status_code, 422, response.json)
    
    def test_user_election_document_data(self):
        user = {
            'email': 'testando2@teste.com',
            'name': 'Teste',
            'surname': 'Familia Teste',
            'election_document': {
                'election_number': '665225860159',
                'zone': '001',
                'section': '0110',
                'expedition_date': 'Tue, 02 Apr 2013 10:29:13 GMT'
            }
        }
        response = self.test_client.post('person', data=json.dumps(user), headers=self.headers)
        self.assertEqual(response.status_code, 201, response.json)


if __name__ == '__main__':
    unittest.main(verbosity=3)
