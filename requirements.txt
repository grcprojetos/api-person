Cerberus==1.2
certifi==2018.8.24
Click==7.0
deepdiff==3.3.0
dotenv==0.0.5
Eve==0.8.1
eve-healthcheck==0.2.0
Events==0.3
Flask==1.0.2
Flask-Cors==3.0.7
itsdangerous==1.1.0
Jinja2==2.10
jsonpickle==1.1
MarkupSafe==1.1.1
py-healthcheck==1.7.2
pymongo==3.7.2
python-dotenv==0.10.1
simplejson==3.16.0
six==1.12.0
Werkzeug==0.14.1
wincertstore==0.2
