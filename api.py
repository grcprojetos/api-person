"""
Root file to EduQC's API

The resoucers available should be defined into settings.py
"""

from os import environ
from datetime import datetime
import json
import logging
import traceback
from dotenv import load_dotenv, find_dotenv
from eve import Eve
from eve.auth import BasicAuth
from eve.auth import requires_auth
from flask import request, abort, jsonify
from flask_cors import CORS
from eve_healthcheck import EveHealthCheck
from deepdiff import DeepDiff

# import ipdb
# pylint: disable=unused-import
import settings
try:
    load_dotenv(find_dotenv())
except Exception as exception:
    pass


class MyBasicAuth(BasicAuth):
    """ Module to validate the auth user to access the API data"""

    def check_auth(self, username, password, allowed_roles, resource,
                   method):
        return username == environ.get("MONGO_USER") and password == environ.get("MONGO_PASS")


# pylint: disable=method-hidden
class JSONEncoder(json.JSONEncoder):
    """ class to enconde json with bson and datetime """

    def default(self, o):
        """
            parse objet to json
        """
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime):
            return str(o)
        return json.JSONEncoder.default(self, o)
# pylint: enable=method-hidden


app = Eve(auth=MyBasicAuth)
CORS(app)
EveHealthCheck(app, '/healthcheck')


def log_update(resource_name, updates, original):
    """ Logging every update/replace action"""
    diff = DeepDiff(original, updates)
    headers = dict(request.headers)
    for k, i in diff.items():
        if isinstance(i, set):
            diff[k] = [j for j in i]
    diff.pop('type_changes', None)
    log_message = {
        'resource': resource_name,
        'method': request.method,
        'document': original['_id'],
        'user': headers.get('Useremail'),  # header changed from UserEmail
        'action': diff,
        'remote_addr': request.remote_addr,
        # 'environ': request.environ, # too much info
    }
    db_logs = app.data.pymongo().db['logs']
    # import ipdb; ipdb.set_trace()
    db_logs.insert_one(log_message)
    # app.logger.info(log_message)


def log_delete(resource_name, item):
    """ Logging every delete action"""
    headers = dict(request.headers)
    log_message = {
        'resource': resource_name,
        'method': request.method,
        'document': item['_id'],
        'user': headers.get('Useremail'),  # header changed from UserEmail
        'action': None,
        'remote_addr': request.remote_addr,
        # 'environ': request.environ, # too much info
    }
    db_logs = app.data.pymongo().db['logs']
    db_logs.insert_one(log_message)
    # app.logger.info(log_message)


# --- Main program ---
# pylint: disable=no-member
app.on_replace += log_update
app.on_update += log_update
app.on_delete_item += log_delete
# pylint: enable=no-member

if __name__ == '__main__':
    # the default log level is set to WARNING, so
    # we have to explictly set the logging level
    # to INFO to get our custom message logged.
    app.logger.setLevel(logging.INFO)
    app.name = 'api-person'
    app.run(debug=True, host='0.0.0.0', port=8000)
